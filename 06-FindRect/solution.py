prev_line, next_line = input(), input()
rect_count = 0
while next_line[0] != '-':
    prev_char = '-'
    for idx, next_char in enumerate(next_line):
        if next_char == '#' and prev_char != '#' and prev_line[idx] != '#':
            rect_count += 1
        prev_char = next_char
    prev_line, next_line = next_line, input()
print(rect_count)
