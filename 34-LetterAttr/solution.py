class LetterAttr:
    def __getattr__(self, item):
        return self.__dict__.setdefault(item, item)

    def __setattr__(self, key, value):
        kkey = frozenset(key)
        self.__dict__[key] = ''.join(c for c in value if c in kkey)
        return self.__dict__[key]
