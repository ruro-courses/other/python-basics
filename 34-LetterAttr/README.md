### Буквенное поле

Написать класс LetterAttr, в котором будут допустимы поля с любым
именем; значение каждого поля по умолчанию будет совпадать с именем поля
(строка), а при задании нового строкового значения туда будут попадать
только буквы, встречающиеся в имени поля.

### Examples

#### Input

    A = LetterAttr()
    print(A.letter)
    print(A.digit)
    A.letter = "teller"
    print(A.letter)
    A.letter = "fortune teller"
    print(A.letter)

#### Output

    letter
    digit
    teller
    rteteller

### Submit a solution
