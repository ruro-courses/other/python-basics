from pytest_common import imp


def test_letterattr1():
    LetterAttr = imp(__file__, 'solution.py', items='LetterAttr')
    la = LetterAttr()
    assert la.letter == 'letter'
    assert la.digit == 'digit'
    la.letter = 'teller'
    assert la.letter == 'teller'
    la.letter = 'fortune teller'
    assert la.letter == 'rteteller'


def test_letterattr2():
    LetterAttr = imp(__file__, 'solution.py', items='LetterAttr')
    la = LetterAttr()
    assert la.Q == 'Q'
    assert la.___ == '___'
    la.Q = "QqQq"
    assert la.Q == 'QQ'
    la.Base = "END"
    assert la.Base == ''
