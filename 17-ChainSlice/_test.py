from itertools import (
    combinations,
    combinations_with_replacement,
    count,
    cycle,
    permutations,
    product,
    repeat,
)

from pytest_common import imp, pytest


@pytest.mark.parametrize(
    "begin, end, seqs, values",
    [
        (17, 33, [
            range(7), range(8), range(6), range(9), range(5)
        ], [
             2, 3, 4, 5, 0, 1, 2, 3, 4, 5, 6, 7, 8, 0, 1, 2
         ]),
        (7, 43, [
            combinations('ABCD', 2), combinations_with_replacement('ABCD', 2),
            permutations('ABCD', 2), product('ABCD', repeat=2)
        ], [
             ('A', 'B'), ('A', 'C'), ('A', 'D'), ('B', 'B'), ('B', 'C'),
             ('B', 'D'), ('C', 'C'), ('C', 'D'), ('D', 'D'), ('A', 'B'),
             ('A', 'C'), ('A', 'D'), ('B', 'A'), ('B', 'C'), ('B', 'D'),
             ('C', 'A'), ('C', 'B'), ('C', 'D'), ('D', 'A'), ('D', 'B'),
             ('D', 'C'), ('A', 'A'), ('A', 'B'), ('A', 'C'), ('A', 'D'),
             ('B', 'A'), ('B', 'B'), ('B', 'C'), ('B', 'D'), ('C', 'A'),
             ('C', 'B'), ('C', 'C'), ('C', 'D'), ('D', 'A'), ('D', 'B'),
             ('D', 'C')
         ]),
        (17, 33, [cycle("FooBar")],
         [
             'r', 'F', 'o', 'o', 'B', 'a', 'r', 'F', 'o', 'o', 'B', 'a', 'r',
             'F', 'o', 'o'
         ]),
        (17, 33, [
            repeat("Qq", 10), range(50, 2), count(100, 3), cycle("Buzz")
        ], [
            121, 124, 127, 130, 133, 136, 139, 142, 145, 148, 151, 154, 157,
            160, 163, 166
        ])
    ]
)
def test_chainslice(begin, end, seqs, values):
    chainslice = imp(__file__, 'solution.py', items='chainslice')
    result = list(chainslice(begin, end, *seqs))
    assert result == values
