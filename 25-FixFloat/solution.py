def fix(n):
    def _vfix(v):
        return round(v, n) if isinstance(v, float) else v

    def _deco(f):
        from functools import wraps
        @wraps(f)
        def _f(*args, **kwargs):
            return _vfix(f(
                *(_vfix(v) for v in args),
                **{k: _vfix(v) for k, v in kwargs.items()}
            ))

        return _f

    return _deco
