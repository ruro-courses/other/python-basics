import sys
import math


def get_ordered_pairs(ordered_seq):
    for i in range(1, len(ordered_seq)):
        yield ordered_seq[i], [(pc, name) for _, pc, name in ordered_seq[:i]]


def dist(p1, p2):
    return sum((c1 - c2) ** 2 for c1, c2 in zip(p1, p2))


def get_dists_to(points, target):
    for pc, name in points:
        yield dist(target, pc), pc, name


def find_furthest(points):
    centroid = tuple(
        sum(c[i] for c, _ in galaxies) / len(galaxies)
        for i in range(3)
    )
    points = sorted(
        get_dists_to(points, centroid),
        key=lambda d: d[0]
    )[::-1]

    best_dist, best_pair = 0, (None, None)
    max_center_dist = points[0][0]

    for next_point, other_points in get_ordered_pairs(points):
        next_center_dist, next_coords, next_name = next_point

        # Triangle inequality
        if best_dist > (
                math.sqrt(next_center_dist) +
                math.sqrt(max_center_dist)
        ) ** 2:
            break

        candidate_dist, candidate_coords, candidate_name = max(
            get_dists_to(other_points, next_coords),
            key=lambda d: d[0]
        )
        if candidate_dist > best_dist:
            best_dist, best_pair = candidate_dist, (next_name, candidate_name)

    return best_pair


lines = [
            l.rstrip('\r\n').split(' ')
            for l in iter(sys.stdin.readline, '')
        ][:-1]

galaxies = [
    (tuple(map(float, coords)), name)
    for *coords, name in lines
]

print(*sorted(find_furthest(galaxies)))
