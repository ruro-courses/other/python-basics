from math import ceil, log2, isclose

num = int(input())
for power in range(2, 1 + ceil(log2(num))):
    root = num ** (1 / power)
    if isclose(root, round(root)):
        print("YES")
        break
else:
    print("NO")
