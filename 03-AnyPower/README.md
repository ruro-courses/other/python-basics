### Какая-нибудь степень

Ввести небольшое натуральное число 2⩽N⩽1000000 и проверить, является ли
оно степенью натурального числа (\>1). Вывести YES и NO соответственно.

### Examples

#### Input

    1024

#### Output

    YES

### Submit a solution
