from pytest_common import run, pytest
from math import ceil, log2, sqrt
from random import sample, seed


MAX_NUMBER = 50000
seed(42)

MAX_POWER = ceil(log2(MAX_NUMBER))
MAX_BASE = ceil(sqrt(MAX_NUMBER))
powers = set()
for base in range(2, 1+MAX_BASE):
    for power in range(2, 1+MAX_POWER):
        base_to_power = base ** power
        if base_to_power <= MAX_NUMBER:
            powers.add(base_to_power)
others = set(range(2, 1 + MAX_NUMBER)) - powers
others = set(sample(others, len(powers)))


@pytest.mark.parametrize(
    "number, is_power",
    [(num, True) for num in powers] + [(num, False) for num in others]
)
def test_powers(number, is_power):
    is_power = "YES\n" if is_power else "NO\n"
    result = run(__file__, 'solution.py', input=str(number))
    assert result.returncode == 0, result.stderr
    assert result.stdout == is_power
