def construct(last_digit, target_digit=None, carry=0):
    target_digit = last_digit if target_digit is None else target_digit

    carry, next_digit = divmod(last_digit * target_digit + carry, 10)
    if carry == 0 and next_digit == target_digit:
        return last_digit
    else:
        head = construct(next_digit, target_digit=target_digit, carry=carry)
        return 10 * head + last_digit


print(construct(int(input())))
