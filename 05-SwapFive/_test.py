from math import floor, log10

from pytest_common import pytest, run


@pytest.mark.parametrize(
    "digit, value",
    [
        (0, 0),
        (1, 1),
        (2, 105263157894736842),
        (3, 1034482758620689655172413793),
        (4, 102564),
        (5, 102040816326530612244897959183673469387755),
        (6, 1016949152542372881355932203389830508474576271186440677966),
        (7, 1014492753623188405797),
        (8, 1012658227848),
        (9, 10112359550561797752808988764044943820224719)
    ]
)
def test_swapfive(digit, value):
    result = run(__file__, 'solution.py', input=str(digit))
    assert result.returncode == 0, result.stderr
    swap_num = int(result.stdout)

    if value != 0:
        leading_place = 10 ** (1 + floor(log10(swap_num)))
        forward = swap_num + digit * leading_place
        backward = (swap_num * 10 + 1) * digit
        assert forward == backward, (swap_num, digit)

    assert swap_num == value
