def maxfun(seq, *funcs):
    seq = list(seq)
    funcs = funcs[::-1]

    def apply(f):
        return sum(f(v) for v in seq)

    return max(funcs, key=apply)
