# @formatter:off
from math import atan, atanh, ceil, cos, cosh, degrees, exp, fabs, floor
from math import log10, log2, radians, sin, sinh, sqrt, tan, tanh
# @formatter:on

from pytest_common import imp, pytest


def x3(x): return 3 ** x


def x2(x): return 2 ** x


@pytest.mark.parametrize(
    "seq, funcs, value",
    [
        (range(-2, 10), (sin, cos, exp), exp),
        (iter(range(-2, 10)), (sin, cos, exp), exp),
        ([0] * 10, (x3, x2, sin), x2),
        ([x / 10000 for x in range(-100000, 100000)], (x2, x3, exp), x3),
        ([x / 10000 for x in range(-100000, 10000)], (x2, x3, exp), x2),
        ([x / 100000 for x in range(1, 99999)], (
                atan, atanh, ceil, cos, cosh, degrees, exp, fabs, floor,
                log10, log2, radians, sin, sinh, sqrt, tan, tanh
        ), degrees)
    ]
)
def test_maxfun(seq, funcs, value):
    maxfun = imp(__file__, 'solution.py', items='maxfun')
    result = maxfun(seq, *funcs)
    assert result is value
