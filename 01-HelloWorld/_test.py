from pytest_common import run


def test_hello():
    result = run(__file__, 'solution.py', input="test")
    assert result.returncode == 0, result.stderr
    assert result.stdout == "Hello, world\n"
