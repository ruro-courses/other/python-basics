from typing import Callable, List

from pytest_common import imp, pytest


@pytest.mark.parametrize(
    "sausage_args, operations, results",
    [
        [
            [(), ("HAM", "5/6"), ("SPAM.", 1.25)],
            [
                lambda a, b, c: [str(a), str(b), str(c)],
                lambda a, b, c: str(a + b + c),
                lambda a, b, c: [str(b * 2), str(4 * c / 5)],
                lambda a, b, c: [
                    str(b + a / 6 - 5 * c / 4),
                    not (b + a / 6 - 5 * c / 4)
                ],
                lambda a, b, c: [str(a - c), not (a - c)],
            ],
            [
                [
                    (r"/------------" "\\\n"
                     r"|pork!pork!po|" "\n"
                     r"|pork!pork!po|" "\n"
                     r"|pork!pork!po|" "\n"
                     r"\------------/"),
                    (r"/----------|" "\n"
                     r"|HAMHAMHAMH|" "\n"
                     r"|HAMHAMHAMH|" "\n"
                     r"|HAMHAMHAMH|" "\n"
                     r"\----------|"),
                    (r"/------------\/---|" "\n"
                     r"|SPAM.SPAM.SP||SPA|" "\n"
                     r"|SPAM.SPAM.SP||SPA|" "\n"
                     r"|SPAM.SPAM.SP||SPA|" "\n"
                     r"\------------/\---|"),
                ],
                (r"/------------\/------------\/------------\/-|" "\n"
                 r"|pork!pork!po||pork!pork!po||pork!pork!po||p|" "\n"
                 r"|pork!pork!po||pork!pork!po||pork!pork!po||p|" "\n"
                 r"|pork!pork!po||pork!pork!po||pork!pork!po||p|" "\n"
                 r"\------------/\------------/\------------/\-|"),
                [
                    (r"/------------\/--------|" "\n"
                     r"|HAMHAMHAMHAM||HAMHAMHA|" "\n"
                     r"|HAMHAMHAMHAM||HAMHAMHA|" "\n"
                     r"|HAMHAMHAMHAM||HAMHAMHA|" "\n"
                     r"\------------/\--------|"),
                    (r"/------------" "\\\n"
                     r"|SPAM.SPAM.SP|" "\n"
                     r"|SPAM.SPAM.SP|" "\n"
                     r"|SPAM.SPAM.SP|" "\n"
                     r"\------------/")
                ],
                [
                    (r"/|" "\n"
                     r"||" "\n"
                     r"||" "\n"
                     r"||" "\n"
                     r"\|"),
                    True
                ],
                [
                    (r"/|" "\n"
                     r"||" "\n"
                     r"||" "\n"
                     r"||" "\n"
                     r"\|"),
                    True
                ],
            ]
        ],
        [
            [(" ",)],
            [lambda a: str(5.5 * a)],
            [
                (r"/------------\/------------\/----------"
                 r"--\/------------\/------------\/------|" "\n"
                 r"|            ||            ||          "
                 r"  ||            ||            ||      |" "\n"
                 r"|            ||            ||          "
                 r"  ||            ||            ||      |" "\n"
                 r"|            ||            ||          "
                 r"  ||            ||            ||      |" "\n"
                 r"\------------/\------------/\----------"
                 r"--/\------------/\------------/\------|")
            ]
        ],
        [
            [("|",)],
            [lambda a: [str(a / 100), not (a / 100)]],
            [
                [
                    (r"/|" "\n"
                     r"||" "\n"
                     r"||" "\n"
                     r"||" "\n"
                     r"\|"),
                    False
                ],
            ]
        ],
        [
            [("sausage", 3 / 4)],
            [lambda a: str(a * 333 / 111 / 3)],
            [
                (r"/---------|" "\n"
                 r"|sausagesa|" "\n"
                 r"|sausagesa|" "\n"
                 r"|sausagesa|" "\n"
                 r"\---------|")
            ]
        ],
        [
            [(":", 101.5)],
            [lambda a: str(a * 30)],
            [
                (r"/------------" "\\" * 3045 + "\n") +
                3 * (r"|::::::::::::|" * 3045 + "\n") +
                (r"\------------/" * 3045)
            ]
        ]
    ]
)
def test_sausage(sausage_args, operations, results):
    sausage = imp(__file__, 'solution.py', items='sausage')
    sausages = [sausage(*sa) for sa in sausage_args]
    assert len(operations) == len(results)
    operations: List[Callable]
    for operation, result in zip(operations, results):
        assert result == operation(*sausages), result
