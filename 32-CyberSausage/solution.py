import math


# noinspection PyPep8Naming
class sausage:
    chain_len = 12
    chain_height = 3

    skin_t = skin_b = '-'
    skin_l = skin_r = skin_cut = '|'
    skin_tl = skin_br = '/'
    skin_bl = skin_tr = '\\'
    skin_style = {
        'top': skin_tl + chain_len * skin_t + skin_tr,
        'bot': skin_bl + chain_len * skin_b + skin_br,
    }

    def __init__(
            self, meat='pork!', volume=1.0,
            *, _full_meat=None, _true_volume=None
    ):
        if _full_meat is None:
            full_meat = (f"{sausage.skin_l}"
                         f"{(meat * sausage.chain_len)[:sausage.chain_len]}"
                         f"{sausage.skin_r}")
        else:
            full_meat = _full_meat

        if _true_volume is None:
            if isinstance(volume, str):
                # come on, stop making me use eval
                # why should "5/6" be an acceptable input
                volume = eval(volume, None, None)

            if not isinstance(volume, float):
                volume = float(volume)

            true_volume = volume * sausage.chain_len
        else:
            true_volume = _true_volume

        self.full_meat = full_meat
        self.true_volume = true_volume if true_volume > 0 else 0.0
        self.int_length = math.floor(self.true_volume)

    def __len__(self):
        return self.int_length

    def __bool__(self):
        return self.true_volume > 0

    def __str__(self):
        sausage_link = [
            sausage.skin_style['top'],
            *(
                self.full_meat
                for _ in range(sausage.chain_height)
            ),
            sausage.skin_style['bot']
        ]
        full_links, partial_link = divmod(self.int_length, sausage.chain_len)

        def assemble_sausage(s):
            ss = full_links * s
            if partial_link > 0 or not ss:
                ss += s[:1 + partial_link] + sausage.skin_cut
            return ss

        return '\n'.join(
            assemble_sausage(s)
            for s in sausage_link
        )

    def __add__(self, other):
        assert isinstance(other, self.__class__)
        return self.__class__(
            _full_meat=self.full_meat,
            _true_volume=self.true_volume + other.true_volume
        )

    def __sub__(self, other):
        assert isinstance(other, self.__class__)
        return self.__class__(
            _full_meat=self.full_meat,
            _true_volume=self.true_volume - other.true_volume
        )

    def __mul__(self, other):
        assert isinstance(other, (int, float))
        return self.__class__(
            _full_meat=self.full_meat,
            _true_volume=self.true_volume * other
        )

    __rmul__ = __mul__

    def __truediv__(self, other):
        assert isinstance(other, (int, float))
        return self.__class__(
            _full_meat=self.full_meat,
            _true_volume=self.true_volume / other
        )
