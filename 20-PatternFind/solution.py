from itertools import accumulate


def find_substrs(text, substr):
    i = text.find(substr)
    while i != -1:
        yield i
        i = text.find(substr, i + 1)


def check_match(text, pattern, offsets):
    if len(text) < offsets[-1] - 1:
        return False
    for p, o_prev, o_next in zip(pattern, (0,) + offsets[:-1], offsets):
        if text[o_prev:o_next - 1] != p:
            return False
    return True


def search(text, pattern):
    pattern = pattern.split('@')
    leading = 0
    while pattern and text and not pattern[0]:
        leading += 1
        pattern = pattern[1:]
        text = text[1:]

    if not pattern:
        return 0

    offsets = tuple(accumulate(len(p) + 1 for p in pattern))
    for i in find_substrs(text, pattern[0]):
        if check_match(text[i:], pattern, offsets):
            return i
    return -1


print(search(input(), input()))
