#!/bin/sh
if [ "$#" -ne 3 ]
then
    echo "Usage: ./ejudge.sh [SID] [EJSID] [TASK_ID]"
    echo ""
    echo "SID can be obtained by goind to any problem in the contest and"
    echo "copying ejudge.cs.msu.ru/client?SID=[THIS PART]&action=139&prob_id=1"
    echo ""
    echo "EJSID is the session id cookie"
    echo ""
    echo "TASK_ID seems to be consequtively assigned, so the first task in the "
    echo "list has id 1 and so on"
else
    id=$(printf "%02d\n" $3)
    task_folder=$(find . -maxdepth 1 -type d -name "$id-*" -print -quit)
    curl https://ejudge.cs.msu.ru/client\?SID=$1\&action=139\&prob_id=$3 -k --cookie "EJSID=$2" \
        | pcre2grep -Mo "<h3>(.|\n)*</h3>" \
        | pandoc -s -r html -o $task_folder/README.md
    if [ "$(wc -m $task_folder/README.md | grep -o '^[0-9]*')" -le 1 ]
    then
        rm "$task_folder/README.md"
    fi
fi
