_oct_shex_dict = {
    (str(oct_1), str(oct_2)): chr(oct_1 + oct_2 * 8 + ord(" "))
    for oct_1 in range(8)
    for oct_2 in range(8)
}
_oct_xehs_dict = {v: k for k, v in _oct_shex_dict.items()}


def _bit_oct_divmod(v):
    """
    This is a fast equivalent to

    ```
    while v:
        v, r = divmod(v, 64)
        yield chr(r + ord(" "))
    ```

    which relies on the fact, that 2 octets are 6 bits (and 2**6 == 64)
    and uses a lookup table instead of computing chr(r + ord(" "))
    """
    octets = oct(v)[2:][::-1]
    if len(octets) % 2:
        octets += '0'
    octets = iter(octets)

    for oct_pair in zip(octets, octets):
        yield _oct_shex_dict[oct_pair]


def _oct_bit_muladd(s):
    """
    This is a fast equivalent to

    ```
    result = 0
    for c in s:
        result *= 64
        result += ord(c) - ord(" ")
    return oct(result)[2:]
    ```

    which relies on the fact, that 2 octets are 6 bits (and 2**6 == 64)
    and uses a lookup table instead of computing orc(c) - ord(" ")
    """
    for c in s:
        o1, o2 = _oct_xehs_dict[c]
        yield o2
        yield o1


def shex(n):
    if isinstance(n, str):
        return shex(int('1' + n, 2))[1:]
    result = list(_bit_oct_divmod(n))
    return "".join(result[::-1])


def xehs(s, to_binstring=False):
    if to_binstring:
        return bin(xehs('!' + s))[3:]
    else:
        return int(''.join(_oct_bit_muladd(s)), 8)


def _get_freqs(chars):
    return [
        (ch, "1" * idx)
        for idx, ch in enumerate(chars)
    ]


def encode(txt):
    import collections
    freq = _get_freqs(
        ch for ch, count in
        sorted(
            collections.Counter(txt).items(),
            key=lambda kv: kv[::-1],
            reverse=True
        )
    )
    code_map = dict(freq)
    bincode = '0'.join(code_map[c] for c in txt) + '0'
    bincode += '0' * (5 - (len(bincode) - 1) % 6)
    code = shex(bincode)
    return len(txt), ''.join(f for f, _ in freq), code


def decode(length, chars, code):
    if len(chars) <= 1:
        return ''.join(chars * length)
    freq = _get_freqs(chars)
    code_map = dict((v, k) for k, v in freq)
    b_full = xehs(code, to_binstring=True)
    return ''.join(code_map[c] for c in b_full.split('0'))[:length]
