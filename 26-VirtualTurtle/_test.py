from pytest_common import imp, pytest


@pytest.mark.parametrize(
    "start, commands, states",
    [
        (
                ((0, 0), 0),
                "flfrffrffr",
                [
                    (1, 0), (1, 0), (1, 1), (1, 1), (2, 1), (3, 1), (3, 1),
                    (3, 0), (3, -1), (3, -1)
                ]
        ),
        (
                ((0, 0), 0),
                "lrlrlrlrlrl",
                [(0, 0) for _ in range(11)]
        ),
        (
                ((5, 6), 1),
                "flfrffrffr",
                [
                    (5, 7), (5, 7), (4, 7), (4, 7), (4, 8), (4, 9), (4, 9),
                    (5, 9), (6, 9), (6, 9)
                ]
        ),
    ]
)
def test_virtualturtle(start, commands, states):
    turtle = imp(__file__, 'solution.py', items='turtle')
    start_coord, start_direction = start
    agent = turtle(*start)
    assert len(commands) == len(states)
    assert next(agent) == start_coord
    for c, s in zip(commands, states):
        assert agent.send(c) == s
