def none_max(*args):
    return max(v for v in args if v is not None)


def find_max(array):
    best, current = None, 0

    for value in array:
        current += value
        best, current = none_max(best, current), none_max(0, current)

    return best


print(find_max(iter(lambda: int(input()), 0)))
