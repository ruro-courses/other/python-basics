import sys

stats = [set(), set()]
for idx, line in enumerate(iter(sys.stdin.readline, '\n')):
    line = line.rstrip('\r\n')
    if not line:
        break
    stats[idx % 2].update(line)

first, second = map(len, stats)
print("Mumbo" if first > second else "Jumbo")
