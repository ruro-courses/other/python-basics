from functools import reduce
from math import sin

from pytest_common import imp, pytest


def sin_hash(x):
    return int(f"{sin(x + 1):14.13f}"[-5:])


def lin_ord_hash(x):
    return reduce(
        lambda a, b: a * 13 + b,
        (ord(c) % 13 for c in reversed(str(x)))
    )


def short_ord_hash(x):
    return ord(x[0]) * ord(x[-1])


def square_shift_hash(x):
    return x ** 2 >> 8


@pytest.mark.parametrize(
    "seq, hashfun, mod, values",
    [
        [range(-1000000, 1000000, 77), hash, 128, (204, 202)],
        [range(-1000000, 1000000, 77), sin_hash, 128, (260, 112)],
        [range(-1000000, 1000000, 77), lin_ord_hash, 128, (238, 167)],
        [
            (f"<{i / (i + 1)}>" for i in range(100000)),
            lin_ord_hash, 128, (858, 714)
        ],
        [range(0, 10000000, 23), square_shift_hash, 1024, (1276, 362)],
        [
            (f"{x:x}" for x in range(10020, 10000000, 23)),
            short_ord_hash, 1024, (9293, 189)
        ]
    ]
)
def test_checkhash(seq, hashfun, mod, values):
    checkhash = imp(__file__, 'solution.py', items='checkhash')
    result = checkhash(seq, hashfun, mod)
    assert result == values
