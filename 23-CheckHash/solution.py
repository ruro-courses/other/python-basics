def checkhash(seq, f, mod):
    from collections import Counter
    collisions = Counter(f(v) % mod for v in seq).values()
    return max(collisions), min(collisions)
