import sys


def is_connected(edges, start, finish):
    vertices = {v for edge in edges for v in edge}
    if start not in vertices or finish not in vertices:
        return False

    reach = {v: {v} for v in vertices}
    for (v1, v2) in edges:
        reach[v1].add(v2)
        reach[v2].add(v1)

    visited = set()
    while True:
        to_visit = reach[start] - visited
        if not to_visit:
            return False
        if finish in to_visit:
            return True
        for target in to_visit:
            visited.add(target)
            reach[start] |= reach[target]
            reach[target] = reach[start]


lines = [l.rstrip('\r\n').split(' ') for l in iter(sys.stdin.readline, '')]
tunnels, ((cave1,), (cave2,)) = lines[:-2], lines[-2:]
print("YES" if is_connected(tunnels, cave1, cave2) else "NO")
