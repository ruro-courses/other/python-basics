### Карта подземелья

Вводится карта проходимых в обе стороны тоннелей подземлья в виде строк,
содержащих разделённые пробелом названия двух пещер, которые соединяет
соответствующий тоннель. Две последние строки не содержат пробелов ---
это название входа в подземелье и название выхода. Вывести \"YES\", если
из входа можно попасть в выход, и \"NO\" в противном случае. Пары могут
повторяться или содержать одинаковые слова.

### Examples

#### Input

    markers jumping
    jumping guinea
    skiing pre
    markers gauge
    skiing mpeg
    solar jackson
    skiing solar
    guinea gauge
    mpeg honor
    pre honor
    guinea gauge
    pre mpeg
    markers guinea
    markers gauge
    honor mpeg
    markers jumping
    skiing
    jumping

#### Output

    NO

### Submit a solution
