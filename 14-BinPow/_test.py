from pytest_common import imp, pytest


def f(a, b): return a ^ b


@pytest.mark.parametrize(
    "obj, npow, func, value",
    [
        (2, 33, int.__mul__, 2 ** 33),
        ("Se", 7, str.__add__, "SeSeSeSeSeSeSe"),
        (1, 999999, int.__mul__, 1),
        ("", 999999, str.__add__, ""),
        (11234123, 223341, f, 11234123),
    ]
)
def test_binpow(obj, npow, func, value):
    BinPow = imp(__file__, 'solution.py', items='BinPow')
    result = BinPow(obj, npow, func)
    if npow < 100:
        v = obj
        for _ in range(npow - 1):
            v = func(v, obj)
        assert result == v
    assert result == value
