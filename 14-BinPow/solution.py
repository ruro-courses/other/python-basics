def BinPow(a, n, f):
    if n <= 1:
        return a

    half_n, rem_n = divmod(n, 2)
    half = BinPow(a, half_n, f)
    full = f(half, half)
    return f(full, a) if rem_n else full
