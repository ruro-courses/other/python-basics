from pytest_common import pytest, run


@pytest.mark.parametrize(
    "ins, outs",
    [
        [
            [
                r'42',
                r'100500//33',
                r'"Qq!"*(6-2)',
                r'# Здесь ошибка',
                r'3,,5',
                r'10/(8-8)',
                r'"wer"[2]+"qwe"[1]',
                r'"wer"[7]+"qwe"[9]',
                r'1+(2+(3',
                r'a0=5',
                r'b0=7',
                r'# И здесь ошибка',
                r'12N=12',
                r'# И ещё где-то были',
                r'a0+b0*8',
                r'c=b0//2+a0',
                r'd==100',
                r'c+d',
                r'sorted(dir())',
            ],
            [
                r"42",
                r"3045",
                r"Qq!Qq!Qq!Qq!",
                r"invalid syntax (<string>, line 1)",
                r"division by zero",
                r"rw",
                r"string index out of range",
                r"unexpected EOF while parsing (<string>, line 1)",
                r"invalid identifier '12N'",
                r"61",
                r"invalid assignment 'd==100'",
                r"name 'd' is not defined",
                r"['__builtins__', 'a0', 'b0', 'c']",
            ]
        ],
        [
            [
                r'a=2',
                r'A=3',
                r'A+a',
                r'c=1,2,3,4,5',
                r'c[1:3]*3',
                r'c[a/2]',
                r'sorted(i%5 for i in range(13))',
                r'sorted(dir())',
                r'hex(A*8)',
                r'__name__',
            ],
            [
                r"5",
                r"(2, 3, 2, 3, 2, 3)",
                r"tuple indices must be integers or slices, not float",
                r"[0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 4, 4]",
                r"['A', '__builtins__', 'a', 'c']",
                r"0x18",
                r"builtins",
            ]
        ],
        [
            [
                r'E=globals().__setitem__("G", 100500)',
                r'E',
                r'G',
            ],
            [
                r"None",
                r"100500",
            ]
        ]
    ]
)
def test_boldcalc(ins, outs):
    result = run(
        __file__, 'solution.py',
        input='\n'.join(ins) + '\n'
    )
    assert result.returncode == 0, result.stderr
    assert result.stdout.strip().split('\n') == outs, ('wot?', result.stderr)
