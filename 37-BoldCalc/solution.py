import sys


# noinspection PyBroadException
def bold_eval(line, s):
    if line[0] == '#':
        return

    if '=' in line:
        var, expr = line.split('=', 1)
        if not var.isidentifier():
            return f"invalid identifier {var!r}"

        try:
            s[var] = eval(expr, s, s)
        except Exception:
            return f"invalid assignment {line!r}"
    else:
        try:
            return str(eval(line, s, s))
        except Exception as e:
            return e

    return


state = {}
for ln in iter(sys.stdin.readline, ''):
    ln = ln.rstrip('\r\n').strip(' ')
    if not ln or ln == '.':
        break
    res = bold_eval(ln, state)
    if res is not None:
        print(res)
