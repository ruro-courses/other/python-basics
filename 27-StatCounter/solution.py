def statcounter():
    from functools import wraps

    def wrap_fun(orig_fun):
        @wraps(orig_fun)
        def _fun(*args, **kwargs):
            stats[orig_fun] += 1
            return orig_fun(*args, **kwargs)

        return _fun

    stats = {}
    fun = yield stats
    while True:
        if fun in stats:
            fun = yield fun
        else:
            stats[fun] = 0
            fun = yield wrap_fun(fun)
