directions = [(0, 1), (1, 0), (0, -1), (-1, 0)]


def get(rows, cols):
    z = [[-1 for _ in range(cols)] for _ in range(rows)]
    cur_dir = 0
    cur_pos = (0, 0)
    z[0][0] = 0
    for idx in range(1, rows * cols):
        new_pos = (-1, -1)
        while not (
                0 <= new_pos[0] < rows and
                0 <= new_pos[1] < cols and
                z[new_pos[0]][new_pos[1]] == -1
        ):
            cur_dir = cur_dir % 4
            new_pos = (
                cur_pos[0] + directions[cur_dir][0],
                cur_pos[1] + directions[cur_dir][1]
            )
            cur_dir += 1
        cur_dir -= 1

        cur_pos = new_pos
        z[cur_pos[0]][cur_pos[1]] = idx
    return z


m, n = eval(input())
print(
    '\n'.join(
        ' '.join(
            str(value % 10)
            for value in row
        ) for row in get(n, m)
    )
)
