### Размер объекта

Написать декоратор класса sizer, который добавляет в него поле size,
равное длине объекта, если у объекта есть длина, или модулю
целочисленного представления объекта в противном случае (предполагается,
что ошибок нет). Предоставить пользователю возможность произвольно
менять это поле.

### Examples

#### Input

    @sizer
    class S(str): pass

    @sizer
    class N(float): pass

    s = S("QSXWDC")
    n = N(2.718281828459045)
    print(s, n)
    print(s.size, n.size)
    s.size, n.size = "Wait", "what?"
    print(s.size, n.size)

#### Output

    QSXWDC 2.718281828459045
    6 2
    Wait what?

### Submit a solution
