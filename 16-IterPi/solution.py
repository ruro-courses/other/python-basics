def pigen():
    from itertools import count, accumulate

    def leibnitz_step(step):
        return -(2 * (step % 2) - 1) * 4 / (2 * step + 1)

    yield from accumulate(map(leibnitz_step, count()))
