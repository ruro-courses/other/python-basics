from itertools import islice

from pytest_common import imp, pytest


@pytest.mark.parametrize(
    "begin, end, values",
    [
        (0, 4, [
            4.0,
            2.666666666666667,
            3.466666666666667,
            2.8952380952380956,
        ]),
        (10, 20, [
            3.232315809405594,
            3.058402765927333,
            3.2184027659273333,
            3.0702546177791854,
            3.208185652261944,
            3.079153394197428,
            3.200365515409549,
            3.0860798011238346,
            3.1941879092319425,
            3.09162380666784,
        ]),
        (1000010, 1000015, [
            3.1415936535787736,
            3.141591653601773,
            3.1415936535767734,
            3.141591653603773,
            3.1415936535747733,
        ])
    ]
)
def test_pigen(begin, end, values):
    pigen = imp(__file__, 'solution.py', items='pigen')()
    result = list(islice(pigen, begin, end))
    assert result == values
