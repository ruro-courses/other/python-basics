class WeAre:
    _legion = 0

    def __new__(cls):
        WeAre._legion += 1
        return super().__new__(WeAre)

    def __del__(self):
        WeAre._legion -= 1

    count = property(
        fget=lambda self, *_: WeAre._legion,
        fset=lambda self, *_: None,
        fdel=lambda self, *_: None,
        doc="expect us."
    )
