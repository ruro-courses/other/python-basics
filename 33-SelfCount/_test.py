from pytest_common import imp


def test_weare1():
    WeAre = imp(__file__, 'solution.py', items='WeAre')
    a = WeAre()
    assert 1 == a.count
    b, c = WeAre(), WeAre(),
    a.count = 100500
    assert (3, 3, 3) == (a.count, b.count, c.count)
    del b.count
    del b
    assert 2 == a.count


def test_weare2():
    WeAre = imp(__file__, 'solution.py', items='WeAre')
    a, b = WeAre(), WeAre()
    a.count = -1
    del a
    a, b = WeAre(), WeAre()
    del a
    a, b = WeAre(), WeAre()
    del a
    a, b = WeAre(), WeAre()
    del a
    a, b = WeAre(), WeAre()
    del a
    a, b = WeAre(), WeAre()
    del a
    a, b = WeAre(), WeAre()
    del a
    a, b = WeAre(), WeAre()
    del b.count
    del a
    assert 1 == b.count


def test_weare3():
    WeAre = imp(__file__, 'solution.py', items='WeAre')

    def fun(n):
        res = []
        a = WeAre()
        if n > 0:
            res.extend(fun(n - 1))
        res.append(a.count)
        return res

    assert [8, 7, 6, 5, 4, 3, 2, 1] == fun(7)
