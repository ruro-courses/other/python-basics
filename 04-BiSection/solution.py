# noinspection PyUnresolvedReferences
from math import *  # noqa: F403, F401


def sign(x):
    return (x > 0) - (x < 0)


def bisection_search(f, left, right, abs_tol=1e-07):
    val_left = f(left)
    val_right = f(right)

    center = (left + right) / 2
    dist = abs(right - left)

    while dist >= abs_tol:
        val_center = f(center)

        sign_val_center = sign(val_center)
        if sign_val_center != sign(val_left):
            right, val_right = center, val_center
        elif sign_val_center != sign(val_right):
            left, val_left = center, val_center
        else:
            # Either one of the values reached 0 or the function has no roots
            break

        center = (left + right) / 2
        dist /= 2

    return min(left, right, center, key=lambda x: abs(f(x)))


fun = None
exec("""
def fun(x):
    return {}
""".format(input().strip()))
begin, end = eval(input())
print("{:.8f}".format(bisection_search(fun, begin, end)))
