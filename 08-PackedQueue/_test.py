from pytest_common import pytest, run


@pytest.mark.parametrize(
    "vin, vout",
    [
        (
                '("QWE",1.1,234),2,(None,7),0,2,(7,7,7),2,(12,),(),3,(5,6),3,1'
                '00500',
                [
                    "('QWE', 1.1)",
                    "()",
                    "(234, None)",
                    "(7, 7)",
                    "(7, 7, 12)",
                ]
        ),
        (
                '("QWE",1.1,234),3,0,(),0,(1,2),0,2,0,1,(1,2,3,4),4',
                [
                    "('QWE', 1.1, 234)",
                    "()",
                    "()",
                    "()",
                    "(1, 2)",
                    "()",
                ]
        ),
        (
                "(33, 'blacks', -28, -93, 'eddie', 'voluntary', 15, 32), 6, ('"
                "films', 43, -81, 'wake'), 6, ('appointment', 'specials', 78, "
                "'burlington', 'aurora'), 4, 1, 0, (), (95, -74, -4, -56, 96, "
                "73, -74), 3, 1, 2, 1, ('church', 61, 53, 9, -91, 'instance', "
                "'declined'), 7, 0, (-54, 'tue', 8, 35, 'carefully'), 3, 2, ()"
                ", 0, (-52, 'landing', 'hunting', 57, -12, -61, 31), 15, 6, ('"
                "ave', 'handled', 'budget', -14, 37, 'bright'), (8,), (-43,), "
                "14, (-41, -61, -63, -8, -49, 71, 32, 10), 11, (-20, -8, 47), "
                "11, (-42, 41, 'kong', 55, 'freeze', 41, -67, -90), ('bound', "
                "22, 'museum'), 16, 11, 11, 8, (-45, -51, -28, -9, 'oriental',"
                " 14, 49, -2), 16, ('real', 'vs', 49, 17, 'benjamin', 65, 'imm"
                "unology', -7), (28, 99, 76, 36, 'jacksonville', 87, -56, -39)"
                ", 24, 3, 10, ('contest', -65, 'shirts'), 7, 6, 6, (-33, -78, "
                "68, 'hobby', 'wednesday'), (19, 20, 'wood', 'journey', -1), 1"
                "7, 9, 3, (18, 'angle', -59, 34, 8, -88, 'ko', 5, 98), 18, 3, "
                "8, 7, (31, 86, 'mia', 'pending', 'retain', 'cos', -74), 15, 9"
                ", 8, 11, 10, 5, 5, 10, 9, 5, 11, ('structures', -58, -9, -22)"
                ", (-4, -51, 47, 'macintosh', 'situated', -27), ('ethics', 'co"
                "nfirm', -59, -56, -88, 'lips', 'rec', -94, 'intensive'), ('ps"
                "ychological', -55, 'my', 35, 0), 29, ('preserve', 16, 'phi', "
                "51, 'hill'), 14, 8, ('ideal', 45, 36, -71, -43, -40, 10), (),"
                " 17, 11, 10, (-78, -14, 'vermont', 47, 'pregnancy', 'aaron'),"
                " ('brazilian', 27, 65, 'secrets', 27), 19, 11, (32, 'twelve',"
                " 59, 44, 17), 13, (), 10, 9, 8, (-50, -72, 57, -21, 3, -6, -2"
                ", 'crude'), 14, 7, 11, 9, (95, -66, 'chip', 21, 'coming', -91"
                ", 'supreme', 'fbi'), (-81, -80, 22, 78, 46), 18, (-49, 49, 73"
                ", 'rail', 61, 'societies', 31, -95), (-7, 69, -4, 'hardcover'"
                ", 49), (70, -7, 43), 19, 10, 10, (62, 'followed', -8, 'psycho"
                "logical', -93, -9, -22, 'broken'), 14, 4, 9, 11, 5, ('de', 50"
                ", 'will', 74, -76, 'magnificent', 15), 15, 9, 9, (16, 'wallet"
                "', 24, -78, 'airfare'), 15, 4, 6, 8, ('fall', -90, 'critics')"
                ", 12, (55, 67), 13, 8, 3, (-56, 37, -82, 'vg', 84, 'canvas', "
                "'merely'), (18, -11, -28), 21, (99, 'paul'), (64, 'conjunctio"
                "n', 28, 69, 'spirituality'), 10, 7, (64, -66, -58, 'original'"
                ", -23, 62), 12, 8, (-55, -90, -20, -67, -83, 87), 9, 5, 3, 3,"
                " 9, 10, 3, 6, 5, (84, -31, 26), 9, 6, 4, 6, 5, (-17, 46, 24, "
                "-55, 'trails', 36, 'packs', 'soap'), ('significantly', -40, '"
                "impose', 40, 98, 46, -58, 13, 'psychiatry'), ('cfr', -88, 64,"
                " 58, 'asthma', -16, -54, -44), 34, 7, (-69, -41, 'beginner', "
                "6, -46, 'quotations', 'variations'), (25, 'affair', 'jelsoft'"
                ", -42), 19, 4, 7, ('minimal',), 10, (-80, -29, 'adapter', 'mb"
                "', -46, 'accepting'), 16, 9, ('relations',), 4, 9, 11, (-29, "
                "-53, -55, -51), (-5,), 14, 9, 11, 4, 7, 11, ('physicians',), "
                "4, 8, ('hans', 33), 7, ('warren', 'vulnerable', 78, 87, 35), "
                "16, ('calcium', 20, 1, -45), (-58, 14), 9, (), 4, 3, 9, ('ont"
                "o', 44, 32, -50), 12, 6, ('serving', 'communicate'), 11, 7, ("
                "88, 'username', 76, 25, -42, -25, 45, 27), 19, (73, -32), 12,"
                " 10, 7, 3, 4, 11, ('harry', 99, 'denial', 87, 70, 90), (-53, "
                "'playstation', -23, 99, 89, -92), 15, (-100, 'monroe', -73, '"
                "portrait', -26, 18, 75, -52), 13, (-52, 'canadian', -5, -65),"
                " 10, 7, 6, 3, 10, 7, 9, 7, 6, 4, ('biz', -17), 8, (-13, 'inst"
                "rumental', 23), 13, ('patch', -66), 6, ('competing', 4, -76, "
                "-12, 87, 'sociology'), 9, (84, 88, -54, 60), (72, 0), 13, ('f"
                "lush', -24, -30, -53, -77), 13, (-55, 77, -97, -17, 'labels',"
                " 31, 35), 18, 8, (-84, 'documented'), 6, (-20, 13, 'login', 6"
                "3, -99, 86), ('old', 'bloggers', -18), 20, 6, 7, 9, 6, ('impo"
                "rted', 7, 79, -39, 3, 5), (74, 18, -55, 'version'), 16, ('det"
                "ermines', 95, 89, 'couple', 34, 'designated', 'treating', 'st"
                "eady', -48), 20, 4, 3, 6, 10, 10, 5, 9, ('average', 80, 'prov"
                "ed', 43, 87, -6), 15, 5, 9, (90,), 7, (49, -88, 'manufacturer"
                "s', 'close', 'rail', 6), 14, 3, ('effectively',), ('pal', 'qu"
                "antity', 'arbitrary'), 11, (-100, -88, -38, -57, 'chance'), ("
                "53, 26, -23, -58, 'de', 59, -100), (81, -81, -12, 54, -15, -3"
                "5, 23), 27, (-33, 'cambodia', 27), 6, 5, 6, 11, ('sandra', -6"
                ", -16, 1), 10, 3, (9, 11, -55, 'ash', 'renewal', 39), (72, -5"
                "3, 60, -95, 'numeric', 46, 18, 19), 22, ('exclusive', 61, 'st"
                "arter', 'aspect', 'enhancing', -32, 'warren', -44, 16), 20, ("
                "'civil', -20, 'pink', 41, 'speed'), 14, 9, 3, 4, ('wave', -73"
                ", -76, -87, -63, 'receipt', 'warranty', -61), 12, 4, 3, 7, 6,"
                " ('mariah', -99, -72, 99, 30, -77, 16), 14, 8, 4, 10, 6, 8, 5"
                ", 7, ('philip', -1, 'report', 68, 81), 12, 5, 10, 3, 4, 10, 1"
                "1, ('m', 96, -78, -25, -12, -10, 37, -69, 15), 20, 6, 8, ('ex"
                "cluded', -79, -7, 89), 14, 5, 3, 11, (55, -7), 12, ('institut"
                "ional', 87, 11, 'metres'), (-55, -65, -86), 11, 6, ('letting'"
                ",), 11, 4, (-92, 44, -50, 81), 8, 8, (-23, 'guests', 7, 6), ("
                "92, 13, -25, 'men', 11, 'provided'), 13, 11, 4, (-78, 'bin', "
                "17, 'und', 'trick', 'thickness', -44), 13, 6, 5, (54, 'filed'"
                ", 53, 89, 'wants', 'erik'), ('lunch', 'shared', 'qld'), 12, 4"
                ", (-89, -35, 83, -99, -97, -9, 40), 10, 9, ('tripadvisor', 'r"
                "ss', 82, 'very', 79, 'iceland', 16, -39), 19, 10, (-65, 94, 4"
                "2, 'request', -10, 'solving', 'mutual'), (16, 50, 0), ('fork'"
                ", 42, 'obtained'), (6, 'episodes', 'msie', -44, -91, 'controv"
                "ersial'), ('james', 'pennsylvania', 'centers', -43, -61, -94,"
                " 30, -65), (45, 66, -93, 'love', -60, -42), 38, ('sd', 'essen"
                "ce', -99), 13, 9, 4, 5, 3, 3, ('impose', -6), 7, (-47,), 4, ("
                "'charitable', 40), (-46, 'consists', 'trinity', 71, -60, 4, -"
                "41, 'delivering'), 14, 7, 10, 11, 8, 5, 5, 4, 8, ('hiv', 'cha"
                "nce', 'prefix', -38, -100, 41, 51, -41), 16, 6, 7, 4, 6, 4, ("
                "), 3, 3, 4, ('assembly', 95, -94, -81, 'bangkok', 'tractor', "
                "-98, 41, 'dead'), ('percent',), 17, 8, 9, 7, 8, (69,), 11, 5,"
                " ('push', 61, 'asn', -43, -49, 'designed', -98, 5, -21), (45,"
                " 'street', 49, 47, 'pn', 'toilet', -21, 23), 28, 10, (33, 'ra"
                "lly', -54, 'benz', -71, -79), 13, ('cartridges',), (-28, 'by'"
                ", -14, 30, 42, -45, -54, 'tabs'), 17, ('nose', 'demonstration"
                "', 'daughter', -90, 51), 9, (-30,), (-12, -50, 'magical', 61,"
                " 69, 37), 13, (13, 92, 84, -10, 61, 'cams', 68), 16, 6, 11, 7"
                ", 8, 11, ('downtown', 'russia', 'surprise', 16, 'actress', -4"
                ", 'migration'), 18, 3, 5, 8, 10, ('catalyst', 80, 'authorized"
                "', 'lisa', 29, 30, 'president'), ('tones', 'traveler', 46, -5"
                "7, 36), (77,), 18, 10, (-78, 66, -47, -43, -23, 81, 'colin', "
                "-96, -31), 18, (-64,), 8, 3, ('pr',), ('promising', -55, 'ble"
                "nd', -35, 'lucy'), 16, 6, 6, 9, (66, -1), 7, 11, ('cups', 'sa"
                "ved', 'gentleman', -69, -32, 26, 94), 10, (99, 96), 8, 3, 4, "
                "10, 5, 4, (-59, -85, 0), 11, 9, 11, 11, 8, (64, -7, 'fitted',"
                " 14), 10, 10, 7, 11, ('pmid', 'participate', -100, -8, -23, 3"
                "8, -60, 73, -38), 12, (30, -62, 96, 'refrigerator'), 15, ('ac"
                "res', 'fence'), 9, ('interested', 92, -32, -78, 72, 79, 'bria"
                "n', 'effectiveness'), 19, 11, ('rising', -44, 62, 'geography'"
                "), (85, 71, -97), 15, (55, 'introduces', 'dc', 21, -27, 79, '"
                "roof', 'finished', 18), ('blowjobs', 'pr', 88, 63, 'covered',"
                " 'premiere', -48, 'equivalent', 35), 23, (56, 37, 78, 'buzz',"
                " -63, 25, 72), (-81, 20, 'cleared', -10, -81, 'span'), (-68, "
                "34, -37), 22, 5, 4, 4, 9, (-32, 'ep', 18), 6, 10, 11, ('threa"
                "d', 'domestic', -25, 'mobility', 'byte'), ('lecture', 'passwo"
                "rds', 79, -31, -17), (-52, 'subjective', 'saw', 16, -26, 'sol"
                "ving'), 21, 6, (-65, 'keywords', 79, 1), 11, (-72, 70, -60), "
                "11, 7, 4, (50, -16, 'latter', 79, 'raises', 80, 'problems', '"
                "webcams', 16), ('profiles', -94), (82, 67), 24, (-99, -69, 'l"
                "ocations', -72, 45), 14, 9, 10, 7, (53, 'advisor', -7, 11, 88"
                ", 'contributors'), ('aerospace', 58, -51, 70), 18, 8, 9, 6, 5"
                ", 7, ('polo', 'rand', 14), 7, 4, (-95, 68, -83, -75), (57, 78"
                ", 'lyrics', -3, 'liechtenstein', -36, 'properties', 'tune'), "
                "(-1, 67, 32), 18, 10, (53, 'commissions'), 6, 10, 10, 4, 11, "
                "('pieces', -94, 'big', 'ipaq'), 13, ('demonstrates', 'generou"
                "s', 85, 91, -78, -63, -70, 44), (-13, -40, 23, 'hot', -6), 20"
                ", 10, (), 10, 10, 3, 9, 9, 9, 10, (88, -60, -12, 'eden', -88,"
                " 39, 59, 'florence', 99), ('buy', 'obvious', 'warranties', -9"
                "7, 'interface', 84, 27, 48, 'outsourcing'), ('couple', -32, '"
                "justin', 'standards', 80, 37, 71, -71), 31, ('hats',), (-45, "
                "22, 'abstracts', 'longest', 'suddenly'), 14, 3, 8, 6, 7, 7, 1"
                "1, 11, ('help', 75, 92, 'communication', -43, -13, 6), (40, -"
                "84, -73, -35, 78, 81, 82, 33), (-19, 'decide', 70, -96, 'tabl"
                "et'), 28, 10, 7, 10, ('days',), 7, 10, 3, 5, (38, 'pledge', '"
                "deficit', 'architecture'), ('tier', -83, 'lip', 'islamic', 'g"
                "uess'), 15, 5, 8, 4, 4, 7, (-1, 29), 6, 9, 4, 3, ('deaths',),"
                " 10, (80, 60), ('illness', -98, 54), 13, 5, 8, (30, -6, 63, -"
                "32, 7), 8, 10, 3, (), 11, (73, 'poison', -81, -51, 'update')",
                [
                    "(33, 'blacks', -28, -93, 'eddie', 'voluntary')",
                    "(15, 32, 'films', 43, -81, 'wake')",
                    "('appointment', 'specials', 78, 'burlington')",
                    "('aurora',)",
                    "()",
                    "(95, -74, -4)",
                    "(-56,)",
                    "(96, 73)",
                    "(-74,)",
                    "('church', 61, 53, 9, -91, 'instance', 'declined')",
                    "()",
                    "(-54, 'tue', 8)",
                    "(35, 'carefully')",
                    "()",
                ]
        )
    ]
)
def test_packedqueue(vin, vout):
    result = run(
        __file__, 'solution.py',
        input=vin
    )
    assert result.returncode == 0, result.stderr
    assert result.stdout.strip() == '\n'.join(vout), result.stderr
