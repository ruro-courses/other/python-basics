from collections import deque

queue = deque()
for cmd in eval(input()):
    if isinstance(cmd, tuple):
        queue.extend(cmd)
    elif isinstance(cmd, int):
        values = deque()
        try:
            for _ in range(cmd):
                values.append(queue.popleft())
        except IndexError:
            break
        print(tuple(values))
