#!/bin/bash
set -uo pipefail

check_task() {
  echo -n "$1: "
  flake_check=$(python -m flake8 "$1")
  flake_status=$?

  test_check=$(python -m pytest -vvxs "$1")
  test_status=$?
  test_skipped=$(((test_status == 5)))

  echo -n " code"
  ((flake_status)) && echo -n "[FAIL]" || echo -n "[OK]"

  echo -n " test"
  ((test_skipped)) && echo -n "[SKIP]"
  ((!test_skipped)) && (
    ((test_status)) && echo -n "[FAIL]" || echo -n "[OK]"
  )

  echo -ne "\n"

  ((flake_status)) && echo -ne "---- flake errors ----\n\n${flake_check}\n\n"
  ((!test_skipped && test_status)) && echo -ne "---- test errors ----\n\n${test_check}\n\n"
}

if [ $# -eq 0 ]; then
  for f in $(find . -maxdepth 1 -type d -name "[0-9][0-9]-*" -printf "%P\n" | sort); do
    check_task "$f"
  done
else
  check_task "$@"
fi
