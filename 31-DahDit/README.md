### Морзянка

Написать класс morse(\"строка\"), экземпляр которого переводит
арифметические выражения в морзянку! Параметр «строка» бывает разных
видов, более подробно описан в подсказках, желающие могут догадаться о
его компонентах по примеру (пример почти полный)

### Examples

#### Input

    print(-+morse())
    print(-++~+-+morse())
    print(--+~-~-++~+++-morse())
    print(--+~-~-++~+++-morse(".-"))
    print(--+~-~-++~+++-morse("..-"))
    print(--+~-~-++~+++-morse("..-|"))
    print(--+~-~-++~+++-morse("dot DOT dash"))
    print(--+~-~-++~+++-morse("ai aui oi "))
    print(--+~-~-++~+++-morse("dot dot dash ///")) 

#### Output

    dah dit.
    dah di dit, di dah dit.
    dah dah dit, dah, dah di dit, di di di dah.
    --. - -.. ...-
    --. - -.. ...-
    --. - -.. ...-|
    dash dash DOT, dash, dash dot DOT, dot dot dot dash.
    oi oi aui, oi, oi ai aui, ai ai ai oi
    dash dash dot, dash, dash dot dot, dot dot dot dash///

### Submit a solution
