# noinspection PyPep8Naming
class morse:
    def __init__(self, fmt="di dit dah"):
        if ' ' in fmt:
            fmt = fmt.split(' ')
            self.msg_end = '.'
            self.wrd_sep = ', '
            self.sym_sep = ' '
        else:
            fmt = list(fmt)
            self.msg_end = ''
            self.wrd_sep = ' '
            self.sym_sep = ''

        self.dot, self.last_dot, fmt = fmt[0], fmt[1], fmt[2:]
        if fmt:
            self.dash, fmt = fmt[0], fmt[1:]
        else:
            self.dash, self.last_dot = self.last_dot, self.dot

        if fmt:
            self.msg_end, = fmt

        self.code = [[]]

    def __neg__(self):
        self.code[-1].append(self.dash)
        return self

    def __pos__(self):
        if not self.code[-1]:
            self.code[-1].append(self.last_dot)
        else:
            self.code[-1].append(self.dot)
        return self

    def __invert__(self):
        self.code.append([])
        return self

    def __str__(self):
        return self.wrd_sep.join(
            self.sym_sep.join(word[::-1])
            for word in self.code[::-1]
        ) + self.msg_end
