import collections
import csv

from pytest_common import imp, pytest


class C:
    x, y, z = 1, 3, 5

    def X(self): return self.x

    def Y(self): return self.y

    def __init__(self, dx, dy, dz):
        self.x = dx
        self.Y = dy
        self.Z = dz


@pytest.mark.parametrize(
    "cls, args, kwargs, cm, cf, om, of",
    [
        (
                C, [6, 7, 8], {},
                "XY", "xyz",
                "", "YZ"
        ),
        (
                collections.Counter, ["qweqweqwe"], {},
                [
                    'clear', 'copy', 'elements', 'fromkeys', 'get', 'items',
                    'keys', 'most_common', 'pop', 'popitem', 'setdefault',
                    'subtract', 'update', 'values'
                ], [],
                [], []
        ),
        (
                csv.DictReader, [""], {},
                [], ['fieldnames'],
                [], ['dialect', 'line_num', 'reader', 'restkey', 'restval']
        )
    ]
)
def test_countfields(cls, args, kwargs, cm, cf, om, of):
    fcounter = imp(__file__, 'solution.py', items='fcounter')
    value = tuple(sorted(v) for v in (cm, cf, om, of))
    result = fcounter(cls, *args, **kwargs)
    assert result == value
