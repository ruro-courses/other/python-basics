### Счётчик полей

Написать функцию fcounter(), которая первым параметром получает
некоторый класс, а остальные параметры применяет для создания экземпляра
этого класса. Функция должна возвращать 4 отсортированных списка: имена
методов класса, имена полей класса, имена методов, которые появились в
экземпляре и имена полей, которые появились в экземпляре (под «полями»
имеются в виду не-callable() объекты).

### Examples

#### Input

    class C:
        x, y, z = 1, 3, 5

        def X(self): return self.x
        def Y(self): return self.y

        def __init__(self, dx, dy, dz):
            self.x = dx
            self.Y = dy
            self.Z = dz

    cm, cf, om, of = fcounter(C, 6, 7, 8)
    print("Class: methods", *cm)
    print("Class: fields", *cf)
    print("Object: methods", *om)
    print("Object: fields", *of)

#### Output

    Class: methods X Y
    Class: fields x y z
    Object: methods
    Object: fields Y Z

### Submit a solution
