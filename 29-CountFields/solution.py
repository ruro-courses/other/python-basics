def fcounter(cls, *args, **kwargs):
    def get_fields(v):
        m, f = set(), set()
        for k in dir(v):
            if not k.startswith('_'):
                (m, f)[not callable(getattr(v, k))].add(k)
        return m, f

    cm, cf = get_fields(cls)
    om, of = get_fields(cls(*args, **kwargs))
    om -= cm
    of -= cf
    return tuple(sorted(v) for v in [cm, cf, om, of])
